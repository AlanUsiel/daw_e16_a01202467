/*
 * Definimos nuestras funciones Java Sccript 
 * en un file .JS externo para ser llamada
 * dentro de mi html posteriormente
 */

/*
 * Problema 1
 * Entrada: un número pedido con un prompt. 
 * Salida: Una tabla con los números del 1 al número dado con sus cuadrados y cubos. 
 * Utiliza document.write para producir la salida
 */

    document.write( "<h1>Problema 1</h2>");
            var num, i, count;
            num = window.prompt("Numero Entero: ");
            //document.write("<br><table><tr><th>Numero</th><th>Cuadrado</th><th>Cubo</th></tr>");
                    //Tabla para numero introducido   
            if(num > 0){
                document.write("<br><table><tr><th>Numero</th><th>Cuadrado</th><th>Cubo</th></tr>");
                    for (i = 0; i < num; i++) {
                        count=i+1;
                        document.write("<tr><td>" + count + "</td>");
                        document.write("<td>" + Math.pow(count,2) + "</td>");
                        document.write("<td>" + Math.pow(count,3) + "</td></tr>");
                    }
                    document.write("</table><br>");
            }
            else{
                document.write("<h4>Me validaro para no hacer operaciones con valores menores o iguales a 0 :) </h4>");
            }
        
/*
 * Problema 2
 * Entrada: Usando un prompt se pide el resultado de la suma de 2 números 
 * generados de manera aleatoria. Salida: La página debe indicar si el resultado 
 * fue correcto o incorrecto, y el tiempo que tardó el usuario en escribir la respuesta
 */
document.write("<h1>Problema 2</h1>");
var x , y, result, init, end, time_elapsed, secs;
/*Generar numero random*/
x = Math.floor(Math.random() * 100);
y = Math.floor(Math.random() * 100);
//Timer
init = Date.now();
result = window.prompt("Resultado de " + x + "+" + y);
    if(result == (x+y) ){
        document.write("!Correcto! <br>");
    }
    else{
        document.write("¡¡¡¡Error!!! <br>");
        document.write("Respuesta: " + (x+y) );                
    }
end = Date.now();
time_elapsed = (end-init)/1000;
time_elapsed = Math.round(time_elapsed);
document.write("<br> <strong> Tiempo total: "+ time_elapsed + "secs </strong>");
        
        
/*
 * Problema 3 
 * Función: contador. Parámetros: Un arreglo de números. 
 * Regresa: La cantidad de números negativos en el arreglo, 
 * la cantidad de 0's, y la cantidad de valores mayores a 0 en el arreglo.
 */
document.write("<h1> Problema 3 </h1>");
var arreglo= new Array(20);
var i, ceros=0, positivos=0, negativos=0;
    for(i=0; i<arreglo.length; i++){
        arreglo[i]=Math.floor( (Math.random()*100)-20);
    }
    for(i=0; i<arreglo.length; i++){
        document.write("Arr["+i+"] = "+arreglo[i]+"<br>" );
    }    
       for(i=0; i<arreglo.length; i++){
            if(arreglo[i] > 0){
                positivos++;
            }
            else if(arreglo[i] == 0){
                ceros++;
            }
            else{
                negativos++;
            }                        
        }
document.write("Total Ceros: "+ ceros +"<br>");
document.write("Total Positivos: "+ positivos +"<br>");
document.write("Total Negativos: "+ negativos +"<br>");
        
/*
 * Problema 4
 * Función: promedios. Parámetros: Un arreglo de arreglos de números. 
 * Regresa: Un arreglo con los promedios de cada uno de los renglones de la matriz.
 */
document.write("<h1> Problema 4 </h1>");
document.write("<h4> Matriz con número aleatorios del 0 al 10</h4>");
var Mat = [];
var i,j,average=0;
for(i=0; i<10; i++){
    document.write("-" +(i+1)+" | ");
}
document.write("<br><br>");
for(i = 0; i < 10; i++){
    Mat[i] = [];
    document.write("--"+(i+1)+" | ");
    for(j = 0; j < 9; j++){ 
        Mat[i][j] = Math.floor((Math.random() * 10));
        average += Mat[i][j];
        document.write(Mat[i][j] + " , ");
    }    
   Mat[i][j] = Math.floor((Math.random() * 10));
   average += Mat[i][j];
   document.write(Mat[i][j] + "---Promedio Renglon["+ (i+1) +" ]= " +  (average/10) + " <br>");
   average=0;
}

/*
 * Problema 5
 * Función: inverso. Parámetros: Un número. Regresa: El número con sus dígitos en orden inverso.
 */
document.write("<h1> Problema 5 </h1>");
var number,x;
number = window.prompt("Ingresa un Número: ");
document.write("Número Ingresado: "+number+"<br>");
document.write("Número Invertido: ");
while(number!=0){
       document.write(number%10);
       number = (number - number%10)/10;
}

/* 
 * Problema 6
 * Crea una solución para un problema de tu elección 
 * (puede ser algo relacionado con tus intereses, 
 * alguna problemática que hayas identificado en algún ámbito, 
 * un problema de programación que hayas resuelto en otro lenguaje,
 * un problema de la ACM, entre otros). El problema debe estar descrito en un documento HTML, 
 * y la solución implementada en JavaScript, utilizando al menos la creación de un objeto, 
 * y el objeto además de su constructor debe tener al menos 2 métodos. Muestra los resultados 
 * en el documento HTML.
 */
document.write("<h1> Problema 6 </h1>");
document.write("<h3> Evaluar tiempos de algoritmos de búqueda</h3>");
var arr= new Array(10000);
var i, size=10000;    

document.write("<h4>Arreglo Random de 10000 números</h4>")
 for(i=0; i<size; i++){
        arr[i]=Math.floor( (Math.random()*100) );
        //document.write(arr[i]+",");
}
document.write("Bubble Sort: ");
//Ordenación Bubble Sort
init = Date.now();
      var swapped = true;
      var j = 0;
      var tmp;
      while (swapped) {
            swapped = false;
            j++;
            for ( i = 0; i < size - j; i++) {
                  if (arr[i] > arr[i + 1]) {
                        tmp = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = tmp;
                        swapped = true;
                  }
            }
      }
end = Date.now();
time_elapsed = (end-init);/*
        for(i=0; i<size; i++){
            document.write(arr[i]+",")
        }*/
document.write("<strong>Tiempo total: "+ time_elapsed + "millisecs </strong> <br>");



//document.write("<h3>Arreglo Random de 10000 números</h3>")
 for(i=0; i<size; i++){
        arr[i]=Math.floor( (Math.random()*100) );
        //document.write(arr[i]+",");
}
//Ordenación Selection Sort
document.write("Selection Sort: ");
init = Date.now();
        var minIndex, tmp, i ,j;    
      for (i = 0; i < size - 1; i++) {
            minIndex = i;
            for (j = i + 1; j < size; j++)
                  if (arr[j] < arr[minIndex])
                        minIndex = j;
            if (minIndex != i) {
                  tmp = arr[i];
                  arr[i] = arr[minIndex];
                  arr[minIndex] = tmp;
            }
      }
end = Date.now();
time_elapsed = (end-init);
/*
for(i=0; i<size; i++){
    document.write(arr[i]+",");
}*/
document.write("<strong> Tiempo total: "+ time_elapsed + "millisecs </strong> <br>");


//Ordenación Insertion Sort
//document.write("<h3>Arreglo Random  de 10000 números</h3>")
 for(i=0; i<size; i++){
        arr[i]=Math.floor( (Math.random()*100) );
       // document.write(arr[i]+",");
}
document.write("Insertion Sort: ");
init = Date.now();
      for (i = 1; i < size; i++) {
            j = i;
            while (j > 0 && arr[j - 1] > arr[j]) {
                  tmp = arr[j];
                  arr[j] = arr[j - 1];
                  arr[j - 1] = tmp;
                  j--;
            }
      }
end = Date.now();
time_elapsed = (end-init);/*
for(i=0; i<size; i++){
    document.write(arr[i]+",");
}*/
document.write("<strong>Tiempo total: "+ time_elapsed +"millisecs </strong>");
